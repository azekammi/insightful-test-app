import { Pipe, PipeTransform } from '@angular/core';
import { Employee } from '../interfaces/employee.interface';

@Pipe({
  name: 'sumOfTotalAmountPaidForRegularHours',
})
export class SumOfTotalAmountPaidForRegularHoursPipe implements PipeTransform {
  transform(employees: Employee[]): number {
    return employees.reduce(
      (acc: number, employee: Employee) =>
        acc + employee.totalAmountPaidForRegularHours,
      0,
    );
  }
}
