import { Pipe, PipeTransform } from '@angular/core';
import { Employee } from '../interfaces/employee.interface';

@Pipe({
  name: 'sumOfTotalOvertimeAmountPaidForOvertimeHours',
})
export class SumOfTotalOvertimeAmountPaidForOvertimeHoursPipe
  implements PipeTransform {
  transform(employees: Employee[]): number {
    return employees.reduce(
      (acc: number, employee: Employee) =>
        acc + employee.totalOvertimeAmountPaidForOvertimeHours,
      0,
    );
  }
}
