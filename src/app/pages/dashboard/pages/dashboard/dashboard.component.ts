import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { combineLatest, filter, finalize } from 'rxjs';
import { LoaderService } from 'src/app/services/loader.service';
import { BulkEditDialogService } from '../../dialogs/bulk-edit/services/bulk-edit-dialog.service';
import { EmployeeDTO } from '../../interfaces/dto/employee.dto.interface';
import { ShiftDTO } from '../../interfaces/dto/shift.dto.interface';
import { Employee } from '../../interfaces/employee.interface';
import { Shift } from '../../interfaces/shift.interface';
import { DataApiService } from '../../services/data.api.service';

@Component({
  selector: 'inta-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
@UntilDestroy()
export class DashboardComponent implements OnInit {
  employees: Employee[] = [];
  shifts: Shift[] = [];

  constructor(
    private readonly dataApiService: DataApiService,
    private readonly loaderService: LoaderService,
    private readonly bulkEditDialogService: BulkEditDialogService,
  ) {}

  ngOnInit() {
    this.getAllData();
  }

  getAllData(): void {
    this.loaderService.increaseLoader();
    combineLatest(
      this.dataApiService.getEmployees(),
      this.dataApiService.getShifts(),
    )
      .pipe(
        finalize(() => this.loaderService.decreaseLoader()),
        untilDestroyed(this),
      )
      .subscribe(([employeesDTO, shiftsDTO]: [EmployeeDTO[], ShiftDTO[]]) => {
        this.shifts = shiftsDTO.map((shiftDTO) => {
          return {
            ...shiftDTO,
            clockInDate: formatDate(
              shiftDTO.clockIn,
              'YYYY-MM-dd',
              'en-GB',
              '+4',
            ),
            clockOutDate: formatDate(
              shiftDTO.clockOut,
              'YYYY-MM-dd',
              'en-GB',
              '+4',
            ),
          };
        });

        this.employees = employeesDTO.map<Employee>(
          (employeeDTO: EmployeeDTO) => {
            const shifts: Shift[] = this.shifts.filter(
              (shift) => shift.employeeId === employeeDTO.id,
            );

            const hoursByShifts: Record<
              string,
              number
            > = this.calcHoursByShifts(shifts);

            return {
              ...employeeDTO,
              shifts: shifts,
              hoursByShifts,
              totalAmountPaidForRegularHours: this.calcTotalAmountPaidForRegularHours(
                hoursByShifts,
                employeeDTO.hourlyRate,
              ),
              totalOvertimeAmountPaidForOvertimeHours: this.calcTotalOvertimeAmountPaidForOvertimeHours(
                hoursByShifts,
                employeeDTO.hourlyRateOvertime,
              ),
            };
          },
        );
      });
  }

  private calcHoursByShifts(shifts: Shift[]): Record<string, number> {
    const hoursByShifts: Record<string, number> = shifts.reduce(
      (hoursByShiftsAcc: Record<string, number>, shift: Shift) => {
        if (!hoursByShiftsAcc[shift.clockInDate]) {
          hoursByShiftsAcc[shift.clockInDate] = 0;
        }

        if (shift.clockInDate === shift.clockOutDate) {
          hoursByShiftsAcc[shift.clockInDate] += this.millisecondsToHours(
            shift.clockOut - shift.clockIn,
          );
        } else {
          hoursByShiftsAcc[shift.clockInDate] += this.millisecondsToHours(
            new Date(`${shift.clockInDate}T23:59:59`).getTime() - shift.clockIn,
          );

          if (!hoursByShiftsAcc[shift.clockOutDate]) {
            hoursByShiftsAcc[shift.clockOutDate] = 0;
          }

          hoursByShiftsAcc[shift.clockOutDate] += this.millisecondsToHours(
            shift.clockOut -
              new Date(`${shift.clockOutDate}T00:00:00`).getTime(),
          );
        }
        return hoursByShiftsAcc;
      },
      {},
    );

    Object.keys(hoursByShifts).forEach((key) => {
      hoursByShifts[key] = this.toFixed(hoursByShifts[key]);
    });

    return hoursByShifts;
  }

  private calcTotalAmountPaidForRegularHours(
    hoursByShifts: Record<string, number>,
    hourlyRate: number,
  ) {
    let totalAmountPaidForRegularHours = 0;
    Object.keys(hoursByShifts).forEach((key) => {
      totalAmountPaidForRegularHours +=
        (hoursByShifts[key] < 8 ? hoursByShifts[key] : 8) * hourlyRate;
    });

    return this.toFixed(totalAmountPaidForRegularHours);
  }

  private calcTotalOvertimeAmountPaidForOvertimeHours(
    hoursByShifts: Record<string, number>,
    hourlyRateOvertime: number,
  ) {
    let totalOvertimeAmountPaidForOvertimeHours = 0;
    Object.keys(hoursByShifts).forEach((key) => {
      totalOvertimeAmountPaidForOvertimeHours +=
        hoursByShifts[key] - 8 > 0
          ? (hoursByShifts[key] - 8) * hourlyRateOvertime
          : 0;
    });

    return this.toFixed(totalOvertimeAmountPaidForOvertimeHours);
  }

  private millisecondsToHours(millisonds: number): number {
    return millisonds / 1000 / 60 / 60;
  }

  private toFixed(value: number): number {
    return parseFloat(value.toFixed(2));
  }

  onBulkEditClick(selectedEmployeeIds: string[]): void {
    this.bulkEditDialogService
      .open(
        this.employees.filter((employee) =>
          selectedEmployeeIds.includes(employee.id),
        ),
      )
      .afterClosed()
      .pipe(
        filter((isEdited) => !!isEdited),
        untilDestroyed(this),
      )
      .subscribe(() => {
        this.getAllData();
      });
  }
}
