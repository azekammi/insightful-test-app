import { ShiftDTO } from './dto/shift.dto.interface';

export interface Shift extends ShiftDTO {
  clockInDate: string;
  clockOutDate: string;
}
