export interface CommonSelectOption {
  label: string;
  value: string;
}
