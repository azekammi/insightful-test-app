import { EmployeeDTO } from './dto/employee.dto.interface';
import { Shift } from './shift.interface';

export interface Employee extends EmployeeDTO {
  shifts: Shift[];
  hoursByShifts: Record<string, number>;
  totalAmountPaidForRegularHours: number;
  totalOvertimeAmountPaidForOvertimeHours: number;
}
