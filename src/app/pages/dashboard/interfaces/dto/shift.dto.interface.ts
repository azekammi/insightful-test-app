export interface ShiftDTO {
  id: string;
  employeeId: string;
  clockIn: number;
  clockOut: number;
}
