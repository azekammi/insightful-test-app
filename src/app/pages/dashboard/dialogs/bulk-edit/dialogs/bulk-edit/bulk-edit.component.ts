import { formatDate } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import { MatTable } from '@angular/material/table';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { combineLatest, finalize, Observable, Subscription } from 'rxjs';
import { EmployeeDTO } from 'src/app/pages/dashboard/interfaces/dto/employee.dto.interface';
import { ShiftDTO } from 'src/app/pages/dashboard/interfaces/dto/shift.dto.interface';
import { Employee } from 'src/app/pages/dashboard/interfaces/employee.interface';
import { Shift } from 'src/app/pages/dashboard/interfaces/shift.interface';
import { DataApiService } from 'src/app/pages/dashboard/services/data.api.service';
import { LoaderService } from 'src/app/services/loader.service';
import { BulkEditDialogData } from '../../interfaces/bulk-edit-dialog-data.interface';

@Component({
  selector: 'inta-bulk-edit',
  templateUrl: './bulk-edit.component.html',
  styleUrls: ['./bulk-edit.component.scss'],
})
@UntilDestroy()
export class BulkEditComponent implements OnInit, OnDestroy {
  mainFormGroup: FormGroup;

  get employeeFormArray(): FormArray {
    return this.mainFormGroup.get('employees') as FormArray;
  }

  get employeeFormGroups(): FormGroup[] {
    return this.employeeFormArray.controls as FormGroup[];
  }

  readonly shiftColumns = ['index', 'clockIn', 'clockOut', 'totalTime'];

  private readonly shiftLimit = 10;

  private subscription: Subscription;

  constructor(
    @Inject(MAT_DIALOG_DATA) public readonly data: BulkEditDialogData,
    private readonly matDialogRef: MatDialogRef<BulkEditComponent>,
    private readonly fb: FormBuilder,
    private readonly dataApiService: DataApiService,
    private readonly loaderService: LoaderService,
  ) {}

  ngOnInit() {
    this.createMainFormGroups();
    this.moveEmployeesToMainFormGroup();
  }

  private createMainFormGroups() {
    this.mainFormGroup = this.fb.group({
      employees: this.fb.array([], Validators.required),
    });
  }

  private moveEmployeesToMainFormGroup() {
    this.data.employees.forEach((employee) => {
      this.employeeFormArray.push(this.createEmployeeFormGroup(employee));
    });
  }

  private createEmployeeFormGroup(employee: Employee) {
    const employeeFormGroup = this.fb.group({
      id: [employee.id, Validators.required],
      name: [employee.name, Validators.required],
      email: [employee.email, Validators.required],
      hourlyRate: [employee.hourlyRate, Validators.required],
      hourlyRateOvertime: [employee.hourlyRateOvertime, Validators.required],
      shifts: this.fb.array([]),
      shiftPage: [1],
      filterDate: [null],
    });

    this.addShiftsToEmployeeFormGroup(employee, employeeFormGroup);

    return employeeFormGroup;
  }

  private addShiftsToEmployeeFormGroup(
    employee: Employee,
    employeeFormGroup: FormGroup,
  ) {
    const page = employeeFormGroup.get('shiftPage')?.value || 1;
    employee.shifts
      .filter(
        (shift) =>
          employeeFormGroup.get('filterDate')?.value === null ||
          shift.clockInDate === employeeFormGroup.get('filterDate')?.value ||
          shift.clockOutDate === employeeFormGroup.get('filterDate')?.value,
      )
      .slice((page - 1) * this.shiftLimit, page * this.shiftLimit)
      .forEach((shift) => {
        (employeeFormGroup.get('shifts') as FormArray).push(
          this.createShiftFormGroup(shift),
        );
      });
  }

  private createShiftFormGroup(shift: Shift) {
    return this.fb.group({
      id: [shift.id, Validators.required],
      employeeId: [shift.employeeId, Validators.required],
      clockIn: [shift.clockIn, Validators.required],
      clockOut: [shift.clockOut, Validators.required],
      clockInHours: [
        formatDate(shift.clockIn, 'H:mm', 'en-GB'),
        Validators.required,
      ],
      clockOutHours: [
        formatDate(shift.clockOut, 'H:mm', 'en-GB'),
        Validators.required,
      ],
      clockInDate: [shift.clockInDate, Validators.required],
      clockOutDate: [shift.clockOutDate, Validators.required],
    });
  }

  onTableScroll(
    e: Event,
    employeeFormGroup: FormGroup,
    matTable: MatTable<Shift>,
  ) {
    const tableWrapper = e.target as HTMLDivElement;
    const tableViewHeight = tableWrapper.offsetHeight;
    const tableScrollHeight = tableWrapper.scrollHeight;
    const scrollLocation = tableWrapper.scrollTop;

    const buffer = 200;
    const limit = tableScrollHeight - tableViewHeight - buffer;
    if (scrollLocation > limit) {
      const employee = this.data.employees.find(
        (employee) => employee.id === employeeFormGroup.getRawValue().id,
      );
      if (employee) {
        employeeFormGroup.patchValue({
          shiftPage: employeeFormGroup.getRawValue().shiftPage + 1,
        });
        this.addShiftsToEmployeeFormGroup(employee, employeeFormGroup);
        matTable.renderRows();
      }
    }
  }

  onDateChange(
    matSelectChange: MatSelectChange,
    employee: Employee,
    employeeFormGroup: FormGroup,
    matTable: MatTable<Shift>,
  ) {
    (employeeFormGroup.get('shifts') as FormArray)?.clear();
    employeeFormGroup.patchValue({
      shiftPage: 1,
      filterDate: matSelectChange.value,
    });
    this.addShiftsToEmployeeFormGroup(employee, employeeFormGroup);
    matTable.renderRows();
  }

  onTimeSet(newTime: string, propName: string, shiftsFormGroup: FormGroup) {
    const clockXDate = shiftsFormGroup.getRawValue()[propName + 'Date'];
    const date = new Date(
      `${clockXDate}T${newTime.split(':')[0]}:${newTime.split(':')[1]}:00`,
    );
    shiftsFormGroup.patchValue({
      [propName]: date.getTime(),
    });
  }

  onSave() {
    if (this.mainFormGroup.invalid) return;

    const observables: Observable<EmployeeDTO | ShiftDTO>[] = [];

    this.employeeFormGroups.forEach((employeeFormGroup) => {
      if (
        employeeFormGroup.get('name')?.dirty ||
        employeeFormGroup.get('hourlyRate')?.dirty ||
        employeeFormGroup.get('hourlyRateOvertime')?.dirty
      ) {
        const { id, ...restEmployeeData } = employeeFormGroup.getRawValue();
        observables.push(
          this.dataApiService.patchEmployee(id, {
            name: restEmployeeData.name,
            email: restEmployeeData.email,
            hourlyRate: parseFloat(restEmployeeData.hourlyRate),
            hourlyRateOvertime: parseFloat(restEmployeeData.hourlyRateOvertime),
          }),
        );
      }

      employeeFormGroup['controls']['shifts']['controls'].forEach(
        (shiftFormGroup: FormGroup) => {
          if (shiftFormGroup.dirty) {
            const { id, ...restShiftData } = shiftFormGroup.getRawValue();
            observables.push(
              this.dataApiService.patchShift(id, {
                employeeId: restShiftData.employeeId,
                clockIn: restShiftData.clockIn,
                clockOut: restShiftData.clockOut,
              }),
            );
          }
        },
      );
    });

    if (observables.length > 0) {
      this.loaderService.increaseLoader();
      this.subscription = combineLatest(...observables)
        .pipe(
          finalize(() => this.loaderService.decreaseLoader()),
          untilDestroyed(this),
        )
        .subscribe(() => {
          this.close(true);
        });
    }
  }

  close(isEdited = false) {
    this.matDialogRef.close(isEdited);
  }

  ngOnDestroy() {
    this.subscription?.unsubscribe();
  }
}
