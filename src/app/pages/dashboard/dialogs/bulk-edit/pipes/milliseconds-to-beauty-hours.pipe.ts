import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'millisecondsToBeautyHours',
})
export class MillisecondsToBeautyHoursPipe implements PipeTransform {
  transform(milliseconds: number): string {
    const totalMinutes = milliseconds / 1000 / 60;
    const hours = Math.floor(totalMinutes / 60);
    const minutes = Math.round(totalMinutes % 60);

    return hours >= 0 && minutes >= 0
      ? `${hours <= 9 ? '0' : ''}${hours}:${minutes <= 9 ? '0' : ''}${minutes}`
      : '-';
  }
}
