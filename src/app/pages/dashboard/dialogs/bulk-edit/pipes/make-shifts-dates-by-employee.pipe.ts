import { Pipe, PipeTransform } from '@angular/core';
import { CommonSelectOption } from '../../../interfaces/common-select-option.interface';
import { Employee } from '../../../interfaces/employee.interface';

@Pipe({
  name: 'makeShiftsDatesByEmployee',
})
export class MakeShiftsDatesByEmployeePipe implements PipeTransform {
  transform(employee: Employee): CommonSelectOption[] {
    return Object.keys(employee.hoursByShifts).map<CommonSelectOption>(
      (date) => ({
        label: date,
        value: date,
      }),
    );
  }
}
