import { Injectable } from '@angular/core';
import {
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
} from '@angular/material/dialog';
import { Employee } from '../../../interfaces/employee.interface';
import { BulkEditComponent } from '../dialogs/bulk-edit/bulk-edit.component';
import { BulkEditDialogData } from '../interfaces/bulk-edit-dialog-data.interface';

@Injectable()
export class BulkEditDialogService {
  private readonly defaultConfigs: MatDialogConfig = {
    width: '100%',
    maxWidth: '850px',
  };
  constructor(private readonly matDialog: MatDialog) {}

  open(
    employees: Employee[],
  ): MatDialogRef<BulkEditComponent, BulkEditDialogData> {
    return this.matDialog.open<BulkEditComponent, BulkEditDialogData>(
      BulkEditComponent,
      {
        ...this.defaultConfigs,
        data: {
          employees,
        },
      },
    );
  }
}
