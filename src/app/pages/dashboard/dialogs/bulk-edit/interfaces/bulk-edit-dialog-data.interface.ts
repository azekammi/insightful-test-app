import { Employee } from '../../../interfaces/employee.interface';

export interface BulkEditDialogData {
  employees: Employee[];
}
