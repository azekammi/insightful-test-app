import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { BulkEditComponent } from './dialogs/bulk-edit/bulk-edit.component';
import { BulkEditDialogService } from './services/bulk-edit-dialog.service';
import { MillisecondsToBeautyHoursPipe } from './pipes/milliseconds-to-beauty-hours.pipe';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { MakeShiftsDatesByEmployeePipe } from './pipes/make-shifts-dates-by-employee.pipe';

@NgModule({
  declarations: [BulkEditComponent, MillisecondsToBeautyHoursPipe, MakeShiftsDatesByEmployeePipe],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    NgxMaterialTimepickerModule,
  ],
  providers: [BulkEditDialogService],
})
export class BulkEditModule {}
