import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Employee } from '../../interfaces/employee.interface';
import { Shift } from '../../interfaces/shift.interface';

@Component({
  selector: 'inta-dashboard-widgets',
  templateUrl: './dashboard-widgets.component.html',
  styleUrls: ['./dashboard-widgets.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardWidgetsComponent {
  @Input() employees: Employee[];
  @Input() shifts: Shift[];
}
