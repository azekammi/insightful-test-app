import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { Employee } from '../../interfaces/employee.interface';

@Component({
  selector: 'inta-employees-table-container',
  templateUrl: './employees-table-container.component.html',
  styleUrls: ['./employees-table-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmployeesTableContainerComponent {
  @Input() set employees(newEmployees: Employee[]) {
    this.innerEmployees = newEmployees;
    this.selectedEmployeeIds = [];
  }

  @Output() bulkEdit: EventEmitter<string[]> = new EventEmitter<string[]>();

  innerEmployees: Employee[] = [];
  selectedEmployeeIds: string[] = [];

  readonly columns: string[] = [
    'checkbox',
    'name',
    'email',
    'totalClockedInATime',
    'totalAmountPaidForRegularHours',
    'totalOvertimeAmountPaidForOvertimeHours',
  ];

  onCheckboxChange(matCheckboxChange: MatCheckboxChange, id: string): void {
    if (matCheckboxChange.checked) {
      this.selectedEmployeeIds.push(id);
    } else {
      const index = this.selectedEmployeeIds.findIndex(
        (selectedEmployeeId) => selectedEmployeeId === id,
      );
      this.selectedEmployeeIds.splice(index, 1);
    }
  }

  onBulkEditClick(): void {
    this.bulkEdit.emit(this.selectedEmployeeIds);
  }
}
