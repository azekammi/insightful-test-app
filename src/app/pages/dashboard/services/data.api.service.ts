import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EmployeeDTO } from '../interfaces/dto/employee.dto.interface';
import { ShiftDTO } from '../interfaces/dto/shift.dto.interface';

@Injectable()
export class DataApiService {
  private readonly baseUrl = 'http://localhost:3000';

  constructor(private httpClient: HttpClient) {}

  getEmployees(): Observable<EmployeeDTO[]> {
    return this.httpClient.get<EmployeeDTO[]>(`${this.baseUrl}/employees`);
  }

  getShifts(): Observable<ShiftDTO[]> {
    return this.httpClient.get<ShiftDTO[]>(`${this.baseUrl}/shifts`);
  }

  patchEmployee(
    id: string,
    payload: Omit<EmployeeDTO, 'id'>,
  ): Observable<EmployeeDTO> {
    return this.httpClient.patch<EmployeeDTO>(
      `${this.baseUrl}/employees/${id}`,
      payload,
    );
  }

  patchShift(id: string, payload: Omit<ShiftDTO, 'id'>): Observable<ShiftDTO> {
    return this.httpClient.patch<ShiftDTO>(
      `${this.baseUrl}/shifts/${id}`,
      payload,
    );
  }
}
