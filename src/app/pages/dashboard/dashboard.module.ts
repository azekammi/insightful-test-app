import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { DashboardRouter } from './dashboard.router';
import { EmployeesTableContainerComponent } from './containers/employees-table-container/employees-table-container.component';
import { DataApiService } from './services/data.api.service';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { InfoWidgetComponent } from 'src/app/components/info-widget/info-widget.component';
import { SumOfTotalAmountPaidForRegularHoursPipe } from './pipes/sum-of-total-amount-paid-for-regular-hours.pipe';
import { SumOfTotalOvertimeAmountPaidForOvertimeHoursPipe } from './pipes/sum-of-total-overtime-amount-paid-for-overtime-hours.pipe';
import { DashboardWidgetsComponent } from './containers/dashboard-widgets/dashboard-widgets.component';
import { BulkEditModule } from './dialogs/bulk-edit/bulk-edit.module';

@NgModule({
  declarations: [
    DashboardComponent,
    EmployeesTableContainerComponent,
    SumOfTotalAmountPaidForRegularHoursPipe,
    SumOfTotalOvertimeAmountPaidForOvertimeHoursPipe,
    DashboardWidgetsComponent,
  ],
  imports: [
    CommonModule,
    DashboardRouter,
    MaterialModule,
    InfoWidgetComponent,
    BulkEditModule,
  ],
  providers: [DataApiService],
})
export class DashboardModule {}
