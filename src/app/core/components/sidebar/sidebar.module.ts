import { NgModule } from '@angular/core';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { SidebarMenuComponent } from './components/sidebar-menu/sidebar-menu.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [SidebarComponent, SidebarMenuComponent],
  imports: [RouterModule],
  exports: [SidebarComponent],
})
export class SidebarModule {}
