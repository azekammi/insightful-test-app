import { NgModule } from '@angular/core';
import { GlobalLoaderComponent } from './components/global-loader/global-loader.component';
import { SpinnerComponent } from './components/spinner/spinner.component';

@NgModule({
  declarations: [GlobalLoaderComponent, SpinnerComponent],
  exports: [GlobalLoaderComponent],
})
export class GlobalLoaderModule {}
