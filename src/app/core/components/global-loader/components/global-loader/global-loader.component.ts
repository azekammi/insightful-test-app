import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'inta-global-loader',
  template:
    '<div class="loader-backdrop"></div><inta-spinner></inta-spinner>',
  styleUrls: ['./global-loader.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GlobalLoaderComponent {}
