import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'inta-header',
  template: '',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {}
