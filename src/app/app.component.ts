import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { LoaderService } from './services/loader.service';

@Component({
  selector: 'inta-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  isLoaderActive$: Observable<boolean> = this.loaderService.getIsLoaderActive();

  constructor(private readonly loaderService: LoaderService) {}
}
