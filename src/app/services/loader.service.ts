import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LoaderService {
  private activeLoaderCount$: BehaviorSubject<number> = new BehaviorSubject<
    number
  >(0);

  private isLoaderActive$: BehaviorSubject<boolean> = new BehaviorSubject<
    boolean
  >(true);

  getIsLoaderActive(): Observable<boolean> {
    return this.isLoaderActive$.asObservable();
  }

  private changeIsLoaderActive(): void {
    this.isLoaderActive$.next(this.activeLoaderCount$.getValue() > 0);
  }

  increaseLoader(): void {
    this.activeLoaderCount$.next(this.activeLoaderCount$.getValue() + 1);

    this.changeIsLoaderActive();
  }

  decreaseLoader(): void {
    this.activeLoaderCount$.next(
      this.activeLoaderCount$.getValue() > 1
        ? this.activeLoaderCount$.getValue() - 1
        : 0,
    );

    this.changeIsLoaderActive();
  }
}
