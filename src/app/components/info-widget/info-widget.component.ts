import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'inta-info-widget',
  standalone: true,
  templateUrl: './info-widget.component.html',
  styleUrls: ['./info-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoWidgetComponent {
  @Input() title: string;
  @Input() value: string;
}
