import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainLayoutComponent } from './layouts/main-layout/main-layout.component';
import { HeaderComponent } from './core/components/header/header.component';
import { SidebarModule } from './core/components/sidebar/sidebar.module';
import { GlobalLoaderModule } from './core/components/global-loader/global-loader.module';
import { LoaderService } from './services/loader.service';
import { DashboardModule } from './pages/dashboard/dashboard.module';

@NgModule({
  declarations: [AppComponent, MainLayoutComponent, HeaderComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    SidebarModule,
    GlobalLoaderModule,
    DashboardModule,
  ],
  providers: [LoaderService],
  bootstrap: [AppComponent],
})
export class AppModule {}
